import logo from "./logo.svg";
import "./App.css";
import ListCars from "./components/ListCars/Cars";
import Protected from "./components/Protected";
import Index from "./pages/index";
import Dashboard from "./pages/Dashboard";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Index />} />

        <Route
          path="/cars"
          element={
            <Protected>
              <Dashboard />
            </Protected>
          }
        />
      </Routes>
    </BrowserRouter>
    // <div className="App" style={{padding: '30px'}}>
    //   <h2> Aplikasi List Cars </h2>
    //   <hr/>
    //   <ListCars/>
    // </div>
  );
}

export default App;
