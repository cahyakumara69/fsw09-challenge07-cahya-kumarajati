import React from "react";
import Jumbotron from "./content/jumbotron"
import Service from "./content/service"
import WhyUs from "./content/whyus"
import GetStart from "./content/getstart"
import FAQ from "./content/faq"
import Carousel from "./content/carousel"
const main = () => {
    return (
        <div>
            <div className="container-header">
                <Jumbotron />
            </div>
            <Service />
            <WhyUs />
            <Carousel />
            <GetStart />
            <FAQ />
        </div>

    )
}

export default main

