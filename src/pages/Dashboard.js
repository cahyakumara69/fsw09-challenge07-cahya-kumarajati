import ListCars from "../components/ListCars/Cars";
import Header from "../components/header";
import Footer from "../components/footer";
import Jumbotron from "../components/content/jumbotron";

function Dashboard() {
  return (
    <div>
      <Header />
      <Jumbotron />
      <ListCars />
      <Footer />
    </div>
  );
}

export default Dashboard;
